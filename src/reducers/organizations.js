// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import * as actions from "../actions"


export function organizationByIdReducer(state = {}, action) {
  switch (action.type) {
  case actions.CREATING_ORGANIZATION.SUCCESS:
  case actions.DELETING_ORGANIZATION.SUCCESS:
  case actions.LOADING_ORGANIZATION.SUCCESS:
  case actions.UPDATING_ORGANIZATION.SUCCESS:
    const {organization} = action
    if (organization) {
      state = {...state}
      if (organization.deleted) delete state[organization.id]
      else state[organization.id] = organization
    }
    return state
  case actions.LOADING_ORGANIZATIONS.SUCCESS:
    const {organizations} = action
    if (organizations) {
      state = {...state}
      for (let organization of organizations) {
        if (organization.deleted) delete state[organization.id]
        else state[organization.id] = organization
      }
    }
    return state
  default:
    return state
  }
}


export function organizationIdsReducer(state = null, action) {
  switch (action.type) {
  case actions.CREATING_ORGANIZATION.SUCCESS:
  case actions.DELETING_ORGANIZATION.SUCCESS:
  case actions.LOADING_ORGANIZATION.SUCCESS:
  case actions.UPDATING_ORGANIZATION.SUCCESS:
    const {organization} = action
    // Optimistic optimizations
    if (organization.deleted) {
      if (state !== null) state = state.filter(id => id !== organization.id)
    } else if (state !== null && !state.includes(organization.id)) {
      state = [organization.id, ...state]
    }
    return state
  case actions.LOADING_ORGANIZATIONS.SUCCESS:
    const {organizations} = action
    return [...organizations.map(organization => organization.id)]
  default:
    return state
  }
}
