// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import * as actions from "../actions"


export function platformByIdReducer(state = {}, action) {
  switch (action.type) {
  case actions.CREATING_PLATFORM.SUCCESS:
  case actions.DELETING_PLATFORM.SUCCESS:
  case actions.LOADING_PLATFORM.SUCCESS:
  case actions.UPDATING_PLATFORM.SUCCESS:
    const {platform} = action
    if (platform) {
      state = {...state}
      if (platform.deleted) delete state[platform.id]
      else state[platform.id] = platform
    }
    return state
  case actions.LOADING_PLATFORMS.SUCCESS:
    const {platforms} = action
    if (platforms) {
      state = {...state}
      for (let platform of platforms) {
        if (platform.deleted) delete state[platform.id]
        else state[platform.id] = platform
      }
    }
    return state
  default:
    return state
  }
}


export function platformIdsReducer(state = null, action) {
  switch (action.type) {
  case actions.CREATING_PLATFORM.SUCCESS:
  case actions.DELETING_PLATFORM.SUCCESS:
  case actions.LOADING_PLATFORM.SUCCESS:
  case actions.UPDATING_PLATFORM.SUCCESS:
    const {platform} = action
    // Optimistic optimizations
    if (platform.deleted) {
      if (state !== null) state = state.filter(id => id !== platform.id)
    } else if (state !== null && !state.includes(platform.id)) {
      state = [platform.id, ...state]
    }
    return state
  case actions.LOADING_PLATFORMS.SUCCESS:
    const {platforms} = action
    return [...platforms.map(platform => platform.id)]
  default:
    return state
  }
}
