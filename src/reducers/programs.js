// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import * as actions from "../actions"


export function programByIdReducer(state = {}, action) {
  switch (action.type) {
  case actions.CREATING_PROGRAM.SUCCESS:
  case actions.DELETING_PROGRAM.SUCCESS:
  case actions.LOADING_PROGRAM.SUCCESS:
  case actions.UPDATING_PROGRAM.SUCCESS:
    const {program} = action
    if (program) {
      state = {...state}
      if (program.deleted) delete state[program.id]
      else state[program.id] = program
    }
    return state
  case actions.LOADING_PROGRAMS.SUCCESS:
    const {programs} = action
    if (programs) {
      state = {...state}
      for (let program of programs) {
        if (program.deleted) delete state[program.id]
        else state[program.id] = program
      }
    }
    return state
  default:
    return state
  }
}


export function programIdsReducer(state = null, action) {
  switch (action.type) {
  case actions.CREATING_PROGRAM.SUCCESS:
  case actions.DELETING_PROGRAM.SUCCESS:
  case actions.LOADING_PROGRAM.SUCCESS:
  case actions.UPDATING_PROGRAM.SUCCESS:
    const {program} = action
    // Optimistic optimizations
    if (program.deleted) {
      if (state !== null) state = state.filter(id => id !== program.id)
    } else if (state !== null && !state.includes(program.id)) {
      state = [program.id, ...state]
    }
    return state
  case actions.LOADING_PROGRAMS.SUCCESS:
    const {programs} = action
    return [...programs.map(program => program.id)]
  default:
    return state
  }
}
