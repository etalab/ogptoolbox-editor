// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {routerReducer} from "react-router-redux"
import {combineReducers} from "redux"
import {reducer as formReducer} from "redux-form"

import {languageReducer} from "./languages"
import {organizationByIdReducer, organizationIdsReducer} from "./organizations"
import {platformByIdReducer, platformIdsReducer} from "./platforms"
import {programByIdReducer, programIdsReducer} from "./programs"
import {tagByIdReducer, tagIdsReducer} from "./tags"
import {usageByIdReducer, usageIdsReducer} from "./usages"
import {authenticationReducer} from "./user"


// Updates the data for different actions.
export default combineReducers({
  authentication: authenticationReducer,
  // ballotById: ballotByIdReducer,
  form: formReducer,
  language: languageReducer,
  organizationById: organizationByIdReducer,
  organizationIds: organizationIdsReducer,
  platformById: platformByIdReducer,
  platformIds: platformIdsReducer,
  programById: programByIdReducer,
  programIds: programIdsReducer,
  tagById: tagByIdReducer,
  tagIds: tagIdsReducer,
  routing: routerReducer,
  usageById: usageByIdReducer,
  usageIds: usageIdsReducer,
})
