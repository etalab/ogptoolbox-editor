// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import * as actions from "../actions"


export function tagByIdReducer(state = {}, action) {
  switch (action.type) {
  case actions.CREATING_TAG.SUCCESS:
  case actions.DELETING_TAG.SUCCESS:
  case actions.LOADING_TAG.SUCCESS:
  case actions.UPDATING_TAG.SUCCESS:
    const {tag} = action
    if (tag) {
      state = {...state}
      if (tag.deleted) delete state[tag.id]
      else state[tag.id] = tag
    }
    return state
  case actions.LOADING_TAGS.SUCCESS:
    const {tags} = action
    if (tags) {
      state = {...state}
      for (let tag of tags) {
        if (tag.deleted) delete state[tag.id]
        else state[tag.id] = tag
      }
    }
    return state
  default:
    return state
  }
}


export function tagIdsReducer(state = null, action) {
  switch (action.type) {
  case actions.CREATING_TAG.SUCCESS:
  case actions.DELETING_TAG.SUCCESS:
  case actions.LOADING_TAG.SUCCESS:
  case actions.UPDATING_TAG.SUCCESS:
    const {tag} = action
    // Optimistic optimizations
    if (tag.deleted) {
      if (state !== null) state = state.filter(id => id !== tag.id)
    } else if (state !== null && !state.includes(tag.id)) {
      state = [tag.id, ...state]
    }
    return state
  case actions.LOADING_TAGS.SUCCESS:
    const {tags} = action
    return [...tags.map(tag => tag.id)]
  default:
    return state
  }
}
