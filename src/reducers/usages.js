// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import * as actions from "../actions"


export function usageByIdReducer(state = {}, action) {
  switch (action.type) {
  case actions.CREATING_USAGE.SUCCESS:
  case actions.DELETING_USAGE.SUCCESS:
  case actions.LOADING_USAGE.SUCCESS:
  case actions.UPDATING_USAGE.SUCCESS:
    const {usage} = action
    if (usage) {
      state = {...state}
      if (usage.deleted) delete state[usage.id]
      else state[usage.id] = usage
    }
    return state
  case actions.LOADING_USAGES.SUCCESS:
    const {usages} = action
    if (usages) {
      state = {...state}
      for (let usage of usages) {
        if (usage.deleted) delete state[usage.id]
        else state[usage.id] = usage
      }
    }
    return state
  default:
    return state
  }
}


export function usageIdsReducer(state = null, action) {
  switch (action.type) {
  case actions.CREATING_USAGE.SUCCESS:
  case actions.DELETING_USAGE.SUCCESS:
  case actions.LOADING_USAGE.SUCCESS:
  case actions.UPDATING_USAGE.SUCCESS:
    const {usage} = action
    // Optimistic optimizations
    if (usage.deleted) {
      if (state !== null) state = state.filter(id => id !== usage.id)
    } else if (state !== null && !state.includes(usage.id)) {
      state = [usage.id, ...state]
    }
    return state
  case actions.LOADING_USAGES.SUCCESS:
    const {usages} = action
    return [...usages.map(usage => usage.id)]
  default:
    return state
  }
}
