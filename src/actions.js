// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


const FAILURE = "FAILURE"
const REQUEST = "REQUEST"
const SUCCESS = "SUCCESS"


function action(type, payload = {}) {
  return {type, ...payload}
}


function createRequestTypes(base) {
  const res = {};
  [REQUEST, SUCCESS, FAILURE].forEach(type => res[type] = `${base}_${type}`)
  return res
}


export const LOAD_AUTHENTICATION_COOKIE = "LOAD_AUTHENTICATION_COOKIE"
export const LOAD_EVERYTHING = "LOAD_EVERYTHING"
export const LOADING_AUTHENTICATION_COOKIE = createRequestTypes("LOADING_AUTHENTICATION_COOKIE")
export const SET_LANGUAGE = "SET_LANGUAGE"
export const SIGN_IN = "SIGN_IN"
export const SIGN_OUT = "SIGN_OUT"
export const SIGN_UP = "SIGN_UP"
export const SIGNING_IN = createRequestTypes("SIGNING_IN")
export const SIGNING_OUT = createRequestTypes("SIGNING_OUT")
export const SIGNING_UP = createRequestTypes("SIGNING_UP")

export const loadingAuthenticationCookie = {
  failure: () => action(LOADING_AUTHENTICATION_COOKIE.FAILURE),
  success: (authentication) => action(LOADING_AUTHENTICATION_COOKIE.SUCCESS, {authentication}),
}

export const signingIn = {
  failure: (values, error) => action(SIGNING_IN.FAILURE, {values, error}),
  request: (values) => action(SIGNING_IN.REQUEST, {values}),
  success: (authentication) => action(SIGNING_IN.SUCCESS, {authentication}),
}

export const signingOut = {
  failure: error => action(SIGNING_OUT.FAILURE, {error}),
  request: () => action(SIGNING_OUT.REQUEST),
  success: () => action(SIGNING_OUT.SUCCESS),
}

export const signingUp = {
  failure: (values, error) => action(SIGNING_UP.FAILURE, {values, error}),
  request: (values) => action(SIGNING_UP.REQUEST, {values}),
  success: (authentication) => action(SIGNING_UP.SUCCESS, {authentication}),
}

export const loadAuthenticationCookie = () => action(LOAD_AUTHENTICATION_COOKIE)
export const loadEverything = (authentication) => action(LOAD_EVERYTHING, {authentication})
export const setLanguage = (language) => action(SET_LANGUAGE, {language})
export const signIn = (values, resolve, reject) => action(SIGN_IN, {values, resolve, reject})
export const signOut = (resolve, reject) => action(SIGN_OUT, {resolve, reject})
export const signUp = (values, resolve, reject) => action(SIGN_UP, {values, resolve, reject})


// Organizations


export const CREATE_ORGANIZATION = "CREATE_ORGANIZATION"
export const CREATING_ORGANIZATION = createRequestTypes("CREATING_ORGANIZATION")
export const DELETE_ORGANIZATION = "DELETE_ORGANIZATION"
export const DELETING_ORGANIZATION = createRequestTypes("DELETING_ORGANIZATION")
export const LOAD_ORGANIZATION = "LOAD_ORGANIZATION"
export const LOAD_ORGANIZATIONS = "LOAD_ORGANIZATIONS"
export const LOADING_ORGANIZATION = createRequestTypes("LOADING_ORGANIZATION")
export const LOADING_ORGANIZATIONS = createRequestTypes("LOADING_ORGANIZATIONS")
export const UPDATE_ORGANIZATION = "UPDATE_ORGANIZATION"
export const UPDATING_ORGANIZATION = createRequestTypes("UPDATING_ORGANIZATION")

export const creatingOrganization = {
  failure: (authentication, values, error) => action(CREATING_ORGANIZATION.FAILURE, {authentication, values, error}),
  request: (authentication, values) => action(CREATING_ORGANIZATION.REQUEST, {authentication, values}),
  success: organization => action(CREATING_ORGANIZATION.SUCCESS, {organization}),
}

export const deletingOrganization = {
  failure: (authentication, id, error) => action(DELETING_ORGANIZATION.FAILURE, {authentication, id, error}),
  request: (authentication, id) => action(DELETING_ORGANIZATION.REQUEST, {authentication, id}),
  success: organization => action(DELETING_ORGANIZATION.SUCCESS, {organization}),
}

export const loadingOrganization = {
  failure: (authentication, id, error) => action(LOADING_ORGANIZATION.FAILURE, {authentication, id, error}),
  request: (authentication, id) => action(LOADING_ORGANIZATION.REQUEST, {authentication, id}),
  success: organization => action(LOADING_ORGANIZATION.SUCCESS, {organization}),
}

export const loadingOrganizations = {
  failure: (authentication, error) => action(LOADING_ORGANIZATIONS.FAILURE, {authentication, error}),
  request: (authentication) => action(LOADING_ORGANIZATIONS.REQUEST, {authentication}),
  success: organizations => action(LOADING_ORGANIZATIONS.SUCCESS, {organizations}),
}

export const updatingOrganization = {
  failure: (authentication, id, values, error) => action(UPDATING_ORGANIZATION.FAILURE, {authentication, id, values, error}),
  request: (authentication, id, values) => action(UPDATING_ORGANIZATION.REQUEST, {authentication, id, values}),
  success: organization => action(UPDATING_ORGANIZATION.SUCCESS, {organization}),
}


export const createOrganization = (authentication, values) => action(CREATE_ORGANIZATION, {authentication, values})
export const deleteOrganization = (authentication, id) => action(DELETE_ORGANIZATION, {authentication, id})
export const loadOrganization = (authentication, id) => action(LOAD_ORGANIZATION, {authentication, id})
export const loadOrganizations = (authentication) => action(LOAD_ORGANIZATIONS, {authentication})
export const updateOrganization = (authentication, id, values) => action(UPDATE_ORGANIZATION, {authentication, id, values})


// Platforms


export const CREATE_PLATFORM = "CREATE_PLATFORM"
export const CREATING_PLATFORM = createRequestTypes("CREATING_PLATFORM")
export const DELETE_PLATFORM = "DELETE_PLATFORM"
export const DELETING_PLATFORM = createRequestTypes("DELETING_PLATFORM")
export const LOAD_PLATFORM = "LOAD_PLATFORM"
export const LOAD_PLATFORMS = "LOAD_PLATFORMS"
export const LOADING_PLATFORM = createRequestTypes("LOADING_PLATFORM")
export const LOADING_PLATFORMS = createRequestTypes("LOADING_PLATFORMS")
export const UPDATE_PLATFORM = "UPDATE_PLATFORM"
export const UPDATING_PLATFORM = createRequestTypes("UPDATING_PLATFORM")

export const creatingPlatform = {
  failure: (authentication, values, error) => action(CREATING_PLATFORM.FAILURE, {authentication, values, error}),
  request: (authentication, values) => action(CREATING_PLATFORM.REQUEST, {authentication, values}),
  success: platform => action(CREATING_PLATFORM.SUCCESS, {platform}),
}

export const deletingPlatform = {
  failure: (authentication, id, error) => action(DELETING_PLATFORM.FAILURE, {authentication, id, error}),
  request: (authentication, id) => action(DELETING_PLATFORM.REQUEST, {authentication, id}),
  success: platform => action(DELETING_PLATFORM.SUCCESS, {platform}),
}

export const loadingPlatform = {
  failure: (authentication, id, error) => action(LOADING_PLATFORM.FAILURE, {authentication, id, error}),
  request: (authentication, id) => action(LOADING_PLATFORM.REQUEST, {authentication, id}),
  success: platform => action(LOADING_PLATFORM.SUCCESS, {platform}),
}

export const loadingPlatforms = {
  failure: (authentication, error) => action(LOADING_PLATFORMS.FAILURE, {authentication, error}),
  request: (authentication) => action(LOADING_PLATFORMS.REQUEST, {authentication}),
  success: platforms => action(LOADING_PLATFORMS.SUCCESS, {platforms}),
}

export const updatingPlatform = {
  failure: (authentication, id, values, error) => action(UPDATING_PLATFORM.FAILURE, {authentication, id, values, error}),
  request: (authentication, id, values) => action(UPDATING_PLATFORM.REQUEST, {authentication, id, values}),
  success: platform => action(UPDATING_PLATFORM.SUCCESS, {platform}),
}


export const createPlatform = (authentication, values) => action(CREATE_PLATFORM, {authentication, values})
export const deletePlatform = (authentication, id) => action(DELETE_PLATFORM, {authentication, id})
export const loadPlatform = (authentication, id) => action(LOAD_PLATFORM, {authentication, id})
export const loadPlatforms = (authentication) => action(LOAD_PLATFORMS, {authentication})
export const updatePlatform = (authentication, id, values) => action(UPDATE_PLATFORM, {authentication, id, values})


// Programs


export const CREATE_PROGRAM = "CREATE_PROGRAM"
export const CREATING_PROGRAM = createRequestTypes("CREATING_PROGRAM")
export const DELETE_PROGRAM = "DELETE_PROGRAM"
export const DELETING_PROGRAM = createRequestTypes("DELETING_PROGRAM")
export const LOAD_PROGRAM = "LOAD_PROGRAM"
export const LOAD_PROGRAMS = "LOAD_PROGRAMS"
export const LOADING_PROGRAM = createRequestTypes("LOADING_PROGRAM")
export const LOADING_PROGRAMS = createRequestTypes("LOADING_PROGRAMS")
export const UPDATE_PROGRAM = "UPDATE_PROGRAM"
export const UPDATING_PROGRAM = createRequestTypes("UPDATING_PROGRAM")

export const creatingProgram = {
  failure: (authentication, values, error) => action(CREATING_PROGRAM.FAILURE, {authentication, values, error}),
  request: (authentication, values) => action(CREATING_PROGRAM.REQUEST, {authentication, values}),
  success: program => action(CREATING_PROGRAM.SUCCESS, {program}),
}

export const deletingProgram = {
  failure: (authentication, id, error) => action(DELETING_PROGRAM.FAILURE, {authentication, id, error}),
  request: (authentication, id) => action(DELETING_PROGRAM.REQUEST, {authentication, id}),
  success: program => action(DELETING_PROGRAM.SUCCESS, {program}),
}

export const loadingProgram = {
  failure: (authentication, id, error) => action(LOADING_PROGRAM.FAILURE, {authentication, id, error}),
  request: (authentication, id) => action(LOADING_PROGRAM.REQUEST, {authentication, id}),
  success: program => action(LOADING_PROGRAM.SUCCESS, {program}),
}

export const loadingPrograms = {
  failure: (authentication, error) => action(LOADING_PROGRAMS.FAILURE, {authentication, error}),
  request: (authentication) => action(LOADING_PROGRAMS.REQUEST, {authentication}),
  success: programs => action(LOADING_PROGRAMS.SUCCESS, {programs}),
}

export const updatingProgram = {
  failure: (authentication, id, values, error) => action(UPDATING_PROGRAM.FAILURE, {authentication, id, values, error}),
  request: (authentication, id, values) => action(UPDATING_PROGRAM.REQUEST, {authentication, id, values}),
  success: program => action(UPDATING_PROGRAM.SUCCESS, {program}),
}


export const createProgram = (authentication, values) => action(CREATE_PROGRAM, {authentication, values})
export const deleteProgram = (authentication, id) => action(DELETE_PROGRAM, {authentication, id})
export const loadProgram = (authentication, id) => action(LOAD_PROGRAM, {authentication, id})
export const loadPrograms = (authentication) => action(LOAD_PROGRAMS, {authentication})
export const updateProgram = (authentication, id, values) => action(UPDATE_PROGRAM, {authentication, id, values})


// Tags


export const CREATE_TAG = "CREATE_TAG"
export const CREATING_TAG = createRequestTypes("CREATING_TAG")
export const DELETE_TAG = "DELETE_TAG"
export const DELETING_TAG = createRequestTypes("DELETING_TAG")
export const LOAD_TAG = "LOAD_TAG"
export const LOAD_TAGS = "LOAD_TAGS"
export const LOADING_TAG = createRequestTypes("LOADING_TAG")
export const LOADING_TAGS = createRequestTypes("LOADING_TAGS")
export const UPDATE_TAG = "UPDATE_TAG"
export const UPDATING_TAG = createRequestTypes("UPDATING_TAG")

export const creatingTag = {
  failure: (authentication, values, error) => action(CREATING_TAG.FAILURE, {authentication, values, error}),
  request: (authentication, values) => action(CREATING_TAG.REQUEST, {authentication, values}),
  success: tag => action(CREATING_TAG.SUCCESS, {tag}),
}

export const deletingTag = {
  failure: (authentication, id, error) => action(DELETING_TAG.FAILURE, {authentication, id, error}),
  request: (authentication, id) => action(DELETING_TAG.REQUEST, {authentication, id}),
  success: tag => action(DELETING_TAG.SUCCESS, {tag}),
}

export const loadingTag = {
  failure: (authentication, id, error) => action(LOADING_TAG.FAILURE, {authentication, id, error}),
  request: (authentication, id) => action(LOADING_TAG.REQUEST, {authentication, id}),
  success: tag => action(LOADING_TAG.SUCCESS, {tag}),
}

export const loadingTags = {
  failure: (authentication, error) => action(LOADING_TAGS.FAILURE, {authentication, error}),
  request: (authentication) => action(LOADING_TAGS.REQUEST, {authentication}),
  success: tags => action(LOADING_TAGS.SUCCESS, {tags}),
}

export const updatingTag = {
  failure: (authentication, id, values, error) => action(UPDATING_TAG.FAILURE, {authentication, id, values, error}),
  request: (authentication, id, values) => action(UPDATING_TAG.REQUEST, {authentication, id, values}),
  success: tag => action(UPDATING_TAG.SUCCESS, {tag}),
}


export const createTag = (authentication, values) => action(CREATE_TAG, {authentication, values})
export const deleteTag = (authentication, id) => action(DELETE_TAG, {authentication, id})
export const loadTag = (authentication, id) => action(LOAD_TAG, {authentication, id})
export const loadTags = (authentication) => action(LOAD_TAGS, {authentication})
export const updateTag = (authentication, id, values) => action(UPDATE_TAG, {authentication, id, values})


// Usages


export const CREATE_USAGE = "CREATE_USAGE"
export const CREATING_USAGE = createRequestTypes("CREATING_USAGE")
export const DELETE_USAGE = "DELETE_USAGE"
export const DELETING_USAGE = createRequestTypes("DELETING_USAGE")
export const LOAD_USAGE = "LOAD_USAGE"
export const LOAD_USAGES = "LOAD_USAGES"
export const LOADING_USAGE = createRequestTypes("LOADING_USAGE")
export const LOADING_USAGES = createRequestTypes("LOADING_USAGES")
export const UPDATE_USAGE = "UPDATE_USAGE"
export const UPDATING_USAGE = createRequestTypes("UPDATING_USAGE")

export const creatingUsage = {
  failure: (authentication, values, error) => action(CREATING_USAGE.FAILURE, {authentication, values, error}),
  request: (authentication, values) => action(CREATING_USAGE.REQUEST, {authentication, values}),
  success: usage => action(CREATING_USAGE.SUCCESS, {usage}),
}

export const deletingUsage = {
  failure: (authentication, id, error) => action(DELETING_USAGE.FAILURE, {authentication, id, error}),
  request: (authentication, id) => action(DELETING_USAGE.REQUEST, {authentication, id}),
  success: usage => action(DELETING_USAGE.SUCCESS, {usage}),
}

export const loadingUsage = {
  failure: (authentication, id, error) => action(LOADING_USAGE.FAILURE, {authentication, id, error}),
  request: (authentication, id) => action(LOADING_USAGE.REQUEST, {authentication, id}),
  success: usage => action(LOADING_USAGE.SUCCESS, {usage}),
}

export const loadingUsages = {
  failure: (authentication, error) => action(LOADING_USAGES.FAILURE, {authentication, error}),
  request: (authentication) => action(LOADING_USAGES.REQUEST, {authentication}),
  success: usages => action(LOADING_USAGES.SUCCESS, {usages}),
}

export const updatingUsage = {
  failure: (authentication, id, values, error) => action(UPDATING_USAGE.FAILURE, {authentication, id, values, error}),
  request: (authentication, id, values) => action(UPDATING_USAGE.REQUEST, {authentication, id, values}),
  success: usage => action(UPDATING_USAGE.SUCCESS, {usage}),
}


export const createUsage = (authentication, values) => action(CREATE_USAGE, {authentication, values})
export const deleteUsage = (authentication, id) => action(DELETE_USAGE, {authentication, id})
export const loadUsage = (authentication, id) => action(LOAD_USAGE, {authentication, id})
export const loadUsages = (authentication) => action(LOAD_USAGES, {authentication})
export const updateUsage = (authentication, id, values) => action(UPDATE_USAGE, {authentication, id, values})
