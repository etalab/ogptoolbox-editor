// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import * as links from "../links"
import {
  userTypesSchema, userTypesUiSchema,
} from "./fields"


export const schema = {
  title: "Online Service",
  type: "object",
  required: [
    "name",
  ],
  properties: {
    name: {
      type: "string",
      description: "Software official, commercial or common name",
      title: "Name",
    },
    description_fr: {
      type: "string",
      title: "Description",
    },
    tags: {
      type: "array",
      title: "Tags",
      items: {
        type: "string",
        enum: links.tagsEnum,
      },
      // uniqueItems: true,
    },
    providers: {
      type: "array",
      title: "Providers",
      items: {
        type: "string",
        enum: links.organizationsEnum,
      },
      // uniqueItems: true,
    },
    uses: {
      type: "array",
      items: {
        type: "string",
        enum: links.programsEnum,
      },
      title: "Uses",
      // uniqueItems: true,
    },
    userTypes: userTypesSchema,
    start: {
      type: "string",
      format: "date",
      title: "Start",
    },
    website: {
      type: "string",
      description: "Official website URL",
      format: "uri",
      title: "Website",
    },
    contactEmail: {
      type: "string",
      format: "email",
      title: "Contact e-mail",
    },
    demo: {
      type: "string",
      description: "URL of demonstration website",
      format: "uri",
      title: "Demo Website",
    },
    logo: {
      type: "string",
      description: "URL of logo image",
      format: "uri",
      title: "Logo",
    },
    screenshots: {
      type: "array",
      description: "URLs of screenshot images",
      items: {
        type: "string",
        format: "uri",
      },
      title: "Screenshots",
      uniqueItems: true,
    },
    languages: {
      type: "array",
      description: "Software localization languages",
      items: {
        type: "string",
      },
      title: "Languages",
      uniqueItems: true,
    },
    clients: {
      type: "array",
      items: {
        type: "string",
      },
      title: "Clients",
      uniqueItems: true,
    },
    twitter: {
      type: "string",
      description: "URL of Twitter page dedicated to program",
      format: "uri"  ,
      title: "Twitter",
    },
    facebook: {
      type: "string",
      description: "URL of Facebook page dedicated to program",
      format: "uri"  ,
      title: "Facebook",
    },
    forum: {
      type: "string",
      description: "URL of users forum dedicated to program",
      format: "uri"  ,
      title: "Forum",
    },
    tos: {
      type: "string",
      description: "URL of TOS page",
      format: "uri"  ,
      title: "Terms of service",
    },
    privacyPolicy: {
      type: "string",
      description: "URL of privacy policy page",
      format: "uri"  ,
      title: "Privacy policy",
    },
    wikipedia_en: {
      type: "string",
      description: "URL of English Wikipedia page for program",
      format: "uri"  ,
      title: "Wikipedia page",
    },
    wikidata: {
      type: "array",
      description: "URL of Wikidata entities describing program",
      items: {
        format: "uri"  ,
        type: "string",
      },
      title: "Wikidata pages",
      uniqueItems: true,
    },
  },
}


export const uiSchema = {
  description_en: {
    "ui:widget": "textarea",
  },
  start: {
    "ui:widget": "alt-date",
  },
  userTypes: userTypesUiSchema,
}
