// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import * as links from "../links"
import {
  userTypesSchema, userTypesUiSchema,
} from "./fields"


export const schema = {
  title: "Usage",
  type: "object",
  required: [
    "name",
  ],
  properties: {
    name: {
      type: "string",
      description: "Usage official, commercial or common name",
      title: "Name",
    },
    description_en: {
      type: "string",
      title: "Description",
    },
    tags: {
      type: "array",
      title: "Tags",
      items: {
        type: "string",
        enum: links.tagsEnum,
      },
      // uniqueItems: true,
    },
    usesPrograms: {
      type: "array",
      items: {
        type: "string",
        enum: links.programsEnum,
      },
      title: "Uses Programs",
      // uniqueItems: true,
    },
    usesPlatforms: {
      type: "array",
      items: {
        type: "string",
        enum: links.platformsEnum,
      },
      title: "Uses Platforms",
      // uniqueItems: true,
    },
    by: {
      type: "array",
      title: "By",
      items: {
        type: "string",
        enum: links.organizationsEnum,
      },
      // uniqueItems: true,
    },
    partners: {
      type: "array",
      title: "Partners",
      items: {
        type: "string",
        enum: links.organizationsEnum,
      },
      // uniqueItems: true,
    },
    location: {
      type: "string",
      title: "Location",
    },
    scale: {
      type: "string",
      description: "City, Country, International...",
      title: "Scale",
    },
    start: {
      type: "string",
      format: "date",
      title: "Start",
    },
    stop: {
      type: "string",
      description: "Date when the initiative has stopped",
      format: "date",
      title: "Stop",
    },
    userTypes: userTypesSchema,
    userSize: {
      type: "number",
      description: "Approximative number of users",
      min: 0,
      multipleOf: 1,
      title: "User count",
    },
    website: {
      type: "string",
      description: "Official website URL",
      format: "uri",
      title: "Website",
    },
    additionalUrls: {
      type: "array",
      items: {
        type: "string",
        format: "uri",
      },
      title: "Additional sites",
      uniqueItems: true,
    },
    repo: {
      type: "string",
      format: "uri",
      title: "Repository",
    },
    contactEmail: {
      type: "string",
      format: "email",
      title: "Contact e-mail",
    },
    twitter: {
      type: "string",
      description: "URL of Twitter page dedicated to initiative",
      format: "uri"  ,
      title: "Twitter",
    },
    facebook: {
      type: "string",
      description: "URL of Facebook page dedicated to initiative",
      format: "uri"  ,
      title: "Facebook",
    },
    clients: {
      type: "array",
      items: {
        type: "string",
      },
      title: "Clients",
      uniqueItems: true,
    },
    wikipedia_en: {
      type: "string",
      description: "URL of English Wikipedia page for program",
      format: "uri"  ,
      title: "Wikipedia page",
    },
  },
}

export const uiSchema = {
  description_en: {
    "ui:widget": "textarea",
  },
  start: {
    "ui:widget": "alt-date",
  },
  stop: {
    "ui:widget": "alt-date",
  },
  userTypes: userTypesUiSchema,
}
