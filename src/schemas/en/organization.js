// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import * as links from "../links"


export const schema = {
  "title": "Organization",
  "type": "object",
  "required": [
    "name",
  ],
  "properties": {
    name: {
      "type": "string",
      "title": "Organization official, commercial or common name",
    },
    description_en: {
      "type": "string",
      "title": "Description",
    },
    type: {
      type: "string",
      enum: [
        "",
        "Administration",
        "Company",
        "Movement",
        "Non-profit organization",
        "Political organization",
        "Political party",
      ],
      title: "Type",
      // uniqueItems: true,
    },
    location: {
      type: "string",
      title: "Location",
    },
    website: {
      type: "string",
      description: "Official website URL",
      format: "uri",
      title: "Website",
    },
    contactEmail: {
      type: "string",
      format: "email",
      title: "Contact e-mail",
    },
    usesPrograms: {
      type: "array",
      items: {
        type: "string",
        enum: links.programsEnum,
      },
      title: "Uses Programs",
      // uniqueItems: true,
    },
    usesPlatforms: {
      type: "array",
      items: {
        type: "string",
        enum: links.platformsEnum,
      },
      title: "Uses Platforms",
      // uniqueItems: true,
    },
    wikipedia_en: {
      type: "string",
      description: "URL of English Wikipedia page for program",
      format: "uri"  ,
      title: "Wikipedia page",
    },
  },
}

export const uiSchema = {
  description_en: {
    "ui:widget": "textarea",
  },
}
