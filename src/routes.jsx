// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {IndexRedirect, IndexRoute, Redirect, Route} from "react-router"

import {setLanguage} from "./actions"
// import About from "./components/about"
import App from "./components/app"
import Home from "./components/home"
import NotFound from "./components/not-found"
import OrganizationDelete from "./components/organizations/delete"
import OrganizationEdit from "./components/organizations/edit"
import Organizations from "./components/organizations/index"
import OrganizationsList from "./components/organizations/list"
import OrganizationNew from "./components/organizations/new"
import OrganizationView from "./components/organizations/view"
import PlatformDelete from "./components/platforms/delete"
import PlatformEdit from "./components/platforms/edit"
import Platforms from "./components/platforms/index"
import PlatformsList from "./components/platforms/list"
import PlatformNew from "./components/platforms/new"
import PlatformView from "./components/platforms/view"
import Profile from "./components/profile"
import ProgramDelete from "./components/programs/delete"
import ProgramEdit from "./components/programs/edit"
import Programs from "./components/programs/index"
import ProgramsList from "./components/programs/list"
import ProgramNew from "./components/programs/new"
import ProgramView from "./components/programs/view"
import SignIn from "./components/sign-in"
import SignOut from "./components/sign-out"
import SignUp from "./components/sign-up"
import TagDelete from "./components/tags/delete"
import TagEdit from "./components/tags/edit"
import Tags from "./components/tags/index"
import TagsList from "./components/tags/list"
import TagNew from "./components/tags/new"
import TagView from "./components/tags/view"
import UsageDelete from "./components/usages/delete"
import UsageEdit from "./components/usages/edit"
import Usages from "./components/usages/index"
import UsagesList from "./components/usages/list"
import UsageNew from "./components/usages/new"
import UsageView from "./components/usages/view"


const i18nRoutes = (
  <Route component={App}>
    <IndexRoute component={Home} />
    <Route component={Organizations} path="organizations">
      <IndexRoute component={OrganizationsList} />
      <Route component={OrganizationNew} path="new" />
      <Route component={OrganizationView} path=":id" />
      <Route component={OrganizationDelete} path=":id/delete" />
      <Route component={OrganizationEdit} path=":id/edit" />
    </Route>
    <Route component={Platforms} path="platforms">
      <IndexRoute component={PlatformsList} />
      <Route component={PlatformNew} path="new" />
      <Route component={PlatformView} path=":id" />
      <Route component={PlatformDelete} path=":id/delete" />
      <Route component={PlatformEdit} path=":id/edit" />
    </Route>
    <Route component={Programs} path="programs">
      <IndexRoute component={ProgramsList} />
      <Route component={ProgramNew} path="new" />
      <Route component={ProgramView} path=":id" />
      <Route component={ProgramDelete} path=":id/delete" />
      <Route component={ProgramEdit} path=":id/edit" />
    </Route>
    <Route component={SignIn} path="sign_in" />
    <Route component={SignOut} path="sign_out" />
    <Route component={SignUp} path="sign_up" />
    <Route component={Tags} path="tags">
      <IndexRoute component={TagsList} />
      <Route component={TagNew} path="new" />
      <Route component={TagView} path=":id" />
      <Route component={TagDelete} path=":id/delete" />
      <Route component={TagEdit} path=":id/edit" />
    </Route>
    <Route component={Usages} path="usages">
      <IndexRoute component={UsagesList} />
      <Route component={UsageNew} path="new" />
      <Route component={UsageView} path=":id" />
      <Route component={UsageDelete} path=":id/delete" />
      <Route component={UsageEdit} path=":id/edit" />
    </Route>
    <Route component={NotFound} isNotFound path="*" />
  </Route>
)


export default function (store) {
  return (
    <Route path="/">
      <IndexRedirect to="en/" />
      <Route path="en" onEnter={() => store.dispatch(setLanguage("en"))}>{i18nRoutes}</Route>
      <Route path="fr" onEnter={() => store.dispatch(setLanguage("fr"))}>{i18nRoutes}</Route>
      <Redirect from="*" to="en/*" />
    </Route>
  )
}