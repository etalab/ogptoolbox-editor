// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import cookie from "react-cookie"
import {browserHistory} from "react-router"
import {END} from "redux-saga"
import {call, fork, put, select, take} from "redux-saga/effects"

import * as actions from "./actions"
import {fetchApiJson, fetchUiJson} from "./fetchers"
import * as links from "./schemas/links"


function getLanguage(state) {
  return state.language
}


function* loadAuthenticationCookie() {
  const authentication = cookie.load("ogptoolbox-editor.authentication")
  if (!authentication) yield put(actions.loadingAuthenticationCookie.failure())
  else yield put(actions.loadingAuthenticationCookie.success(authentication))
}


function* loadEverything(authentication) {
  yield [
    call(loadOrganizations, authentication),
    call(loadPlatforms, authentication),
    call(loadPrograms, authentication),
    call(loadTags, authentication),
    call(loadUsages, authentication),
  ]
}


function* signIn(values, resolve, reject) {
  yield put(actions.signingIn.request(values))
  try {
    const authentication = yield call(fetchUiJson, "api/sign_in", {
      body: JSON.stringify(values),
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
      method: "post",
    })
    const language = yield select(getLanguage)
    yield put(actions.signingIn.success(authentication))
    cookie.save("ogptoolbox-editor.authentication", JSON.stringify(authentication))
    resolve()
    browserHistory.push(`/${language}/`)  // TODO
  } catch (error) {
    yield put(actions.signingIn.failure(values, error))
    if (error.code && error.code >= 400 && error.code < 500) {
      reject({username: error.message || "Authentication failed."})
    } else {
      reject({username: "Authentication failed."})
    }
  }
}


function* signOut(resolve, reject) {
  yield put(actions.signingOut.request())
  try {
    yield call(fetchUiJson, "api/sign_out", {
      body: JSON.stringify({}),
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
      method: "post",
    })
    const language = yield select(getLanguage)
    yield put(actions.signingOut.success())
    cookie.remove("ogptoolbox-editor.authentication")
    resolve()
    browserHistory.push(`/${language}/`)  // TODO
  } catch (error) {
    yield put(actions.signingOut.failure(error))
    reject({})
  }
}


function* signUp(values, resolve, reject) {
  yield put(actions.signingUp.request(values))
  try {
    const authentication = yield call(fetchUiJson, "api/sign_up", {
      body: JSON.stringify(values),
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
      method: "post",
    })
    const language = yield select(getLanguage)
    yield put(actions.signingUp.success(authentication))
    cookie.save("ogptoolbox-editor.authentication", JSON.stringify(authentication))
    resolve()
    browserHistory.push(`/${language}/`)  // TODO
  } catch (error) {
    yield put(actions.signingUp.failure(values, error))
    if (error.code && error.code >= 400 && error.code < 500) {
      reject({username: error.message || "Sign up failed."})
    } else {
      reject({username: "Sign up failed."})
    }
  }
}


function* watchLoadAuthenticationCookie() {
  let action = yield take(actions.LOAD_AUTHENTICATION_COOKIE)
  while (action !== END) {
    yield fork(loadAuthenticationCookie)
    action = yield take(actions.LOAD_AUTHENTICATION_COOKIE)
  }
}


function* watchLoadEverything() {
  let action = yield take(actions.LOAD_EVERYTHING)
  while (action !== END) {
    const {authentication} = action
    yield fork(loadEverything, authentication)
    action = yield take(actions.LOAD_EVERYTHING)
  }
}


function* watchSignIn() {
  let action = yield take(actions.SIGN_IN)
  while (action !== END) {
    const {reject, resolve, values} = action
    yield fork(signIn, values, resolve, reject)
    action = yield take(actions.SIGN_IN)
  }
}


function* watchSignOut() {
  let action = yield take(actions.SIGN_OUT)
  while (action !== END) {
    const {reject, resolve} = action
    yield fork(signOut, resolve, reject)
    action = yield take(actions.SIGN_OUT)
  }
}


function* watchSignUp() {
  let action = yield take(actions.SIGN_UP)
  while (action !== END) {
    const {reject, resolve, values} = action
    yield fork(signUp, values, resolve, reject)
    action = yield take(actions.SIGN_UP)
  }
}


// Organizations


function* createOrganization(authentication, values) {
  yield put(actions.creatingOrganization.request(authentication, values))
  try {
    const organization = yield call(
      fetchApiJson,
      "organizations",
      {
        body: JSON.stringify(values),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "post",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.creatingOrganization.success(organization))
    browserHistory.push(`/${language}/organizations/${organization.id}`)
  } catch (error) {
    yield put(actions.creatingOrganization.failure(authentication, values, error))
  }
}


function* deleteOrganization(authentication, id) {
  yield put(actions.deletingOrganization.request(authentication, id))
  try {
    const organization = yield call(
      fetchApiJson,
      `organizations/${id}`,
      {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "delete",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.deletingOrganization.success(organization))
    browserHistory.push(`/${language}/organizations`)
  } catch (error) {
    yield put(actions.deletingOrganization.failure(authentication, id, error))
  }
}


export function* fetchOrganization(authentication, id) {
  yield put(actions.loadingOrganization.request(authentication, id))
  const authenticationHeaders = authentication && authentication.apiKey ?
    {"OGPToolbox-API-Key": authentication.apiKey} :
    {}
  try {
    const organization = yield call(
      fetchApiJson,
      `organizations/${id}`,
      {
        headers: {
          "Accept": "application/json",
          ...authenticationHeaders,
        },
      },
    )
    yield put(actions.loadingOrganization.success(organization))
  } catch (error) {
    yield put(actions.loadingOrganization.failure(authentication, id, error))
  }
}


export function* fetchOrganizations(authentication) {
  yield put(actions.loadingOrganizations.request(authentication))
  const authenticationHeaders = authentication && authentication.apiKey ?
    {"OGPToolbox-API-Key": authentication.apiKey} :
    {}
  try {
    const organizations = yield call(
      fetchApiJson,
      "organizations",
      {
        headers: {
          "Accept": "application/json",
          ...authenticationHeaders,
        },
      },
    )
    Array.prototype.splice.apply(links.organizationsEnum,
      [0, links.organizationsEnum].concat(organizations.map(organization => organization.name).sort()))
    yield put(actions.loadingOrganizations.success(organizations))
  } catch (error) {
    yield put(actions.loadingOrganizations.failure(authentication, error))
  }
}


function getOrganization(state, id) {
  // return state.organizationById && state.organizationById[id]
  return state.organizationById[id]
}


function getOrganizationIds(state) {
  return state.organizationIds
}


function* loadOrganization(authentication, id) {
  const organization = yield select(getOrganization, id)
  if (!organization) yield call(fetchOrganization, authentication, id)
}


function* loadOrganizations(authentication) {
  const organizationIds = yield select(getOrganizationIds)
  if (organizationIds === null || organizationIds.length === 0) yield call(fetchOrganizations, authentication)
}


function* updateOrganization(authentication, id, values) {
  values = {...values}
  for (let key in values) {
    if (values[key] === null || values[key] === undefined) delete values[key]
  }
  yield put(actions.updatingOrganization.request(authentication, id, values))
  try {
    const organization = yield call(
      fetchApiJson,
      `organizations/${id}`,
      {
        body: JSON.stringify(values),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "put",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.updatingOrganization.success(organization))
    browserHistory.push(`/${language}/organizations/${organization.id}`)
  } catch (error) {
    yield put(actions.updatingOrganization.failure(authentication, id, values, error))
  }
}


function* watchCreateOrganization() {
  let action = yield take(actions.CREATE_ORGANIZATION)
  while (action !== END) {
    const {authentication, values} = action
    yield fork(createOrganization, authentication, values)
    action = yield take(actions.CREATE_ORGANIZATION)
  }
}


function* watchDeleteOrganization() {
  let action = yield take(actions.DELETE_ORGANIZATION)
  while (action !== END) {
    const {authentication, id} = action
    yield fork(deleteOrganization, authentication, id)
    action = yield take(actions.DELETE_ORGANIZATION)
  }
}


function* watchLoadOrganization() {
  let action = yield take(actions.LOAD_ORGANIZATION)
  while (action !== END) {
    const {authentication, id} = action
    yield fork(loadOrganization, authentication, id)
    action = yield take(actions.LOAD_ORGANIZATION)
  }
}


function* watchLoadOrganizations() {
  let action = yield take(actions.LOAD_ORGANIZATIONS)
  while (action !== END) {
    const {authentication} = action
    yield fork(loadOrganizations, authentication)
    action = yield take(actions.LOAD_ORGANIZATIONS)
  }
}


function* watchUpdateOrganization() {
  let action = yield take(actions.UPDATE_ORGANIZATION)
  while (action !== END) {
    const {authentication, id, values} = action
    yield fork(updateOrganization, authentication, id, values)
    action = yield take(actions.UPDATE_ORGANIZATION)
  }
}


// Platforms


function* createPlatform(authentication, values) {
  yield put(actions.creatingPlatform.request(authentication, values))
  try {
    const platform = yield call(
      fetchApiJson,
      "platforms",
      {
        body: JSON.stringify(values),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "post",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.creatingPlatform.success(platform))
    browserHistory.push(`/${language}/platforms/${platform.id}`)
  } catch (error) {
    yield put(actions.creatingPlatform.failure(authentication, values, error))
  }
}


function* deletePlatform(authentication, id) {
  yield put(actions.deletingPlatform.request(authentication, id))
  try {
    const platform = yield call(
      fetchApiJson,
      `platforms/${id}`,
      {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "delete",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.deletingPlatform.success(platform))
    browserHistory.push(`/${language}/platforms`)
  } catch (error) {
    yield put(actions.deletingPlatform.failure(authentication, id, error))
  }
}


export function* fetchPlatform(authentication, id) {
  yield put(actions.loadingPlatform.request(authentication, id))
  const authenticationHeaders = authentication && authentication.apiKey ?
    {"OGPToolbox-API-Key": authentication.apiKey} :
    {}
  try {
    const platform = yield call(
      fetchApiJson,
      `platforms/${id}`,
      {
        headers: {
          "Accept": "application/json",
          ...authenticationHeaders,
        },
      },
    )
    yield put(actions.loadingPlatform.success(platform))
  } catch (error) {
    yield put(actions.loadingPlatform.failure(authentication, id, error))
  }
}


export function* fetchPlatforms(authentication) {
  yield put(actions.loadingPlatforms.request(authentication))
  const authenticationHeaders = authentication && authentication.apiKey ?
    {"OGPToolbox-API-Key": authentication.apiKey} :
    {}
  try {
    const platforms = yield call(
      fetchApiJson,
      "platforms",
      {
        headers: {
          "Accept": "application/json",
          ...authenticationHeaders,
        },
      },
    )
    Array.prototype.splice.apply(links.platformsEnum,
      [0, links.platformsEnum].concat(platforms.map(platform => platform.name).sort()))
    yield put(actions.loadingPlatforms.success(platforms))
  } catch (error) {
    yield put(actions.loadingPlatforms.failure(authentication, error))
  }
}


function getPlatform(state, id) {
  // return state.platformById && state.platformById[id]
  return state.platformById[id]
}


function getPlatformIds(state) {
  return state.platformIds
}


function* loadPlatform(authentication, id) {
  const platform = yield select(getPlatform, id)
  if (!platform) yield call(fetchPlatform, authentication, id)
}


function* loadPlatforms(authentication) {
  const platformIds = yield select(getPlatformIds)
  if (platformIds === null || platformIds.length === 0) yield call(fetchPlatforms, authentication)
}


function* updatePlatform(authentication, id, values) {
  values = {...values}
  for (let key in values) {
    if (values[key] === null || values[key] === undefined) delete values[key]
  }
  yield put(actions.updatingPlatform.request(authentication, id, values))
  try {
    const platform = yield call(
      fetchApiJson,
      `platforms/${id}`,
      {
        body: JSON.stringify(values),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "put",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.updatingPlatform.success(platform))
    browserHistory.push(`/${language}/platforms/${platform.id}`)
  } catch (error) {
    yield put(actions.updatingPlatform.failure(authentication, id, values, error))
  }
}


function* watchCreatePlatform() {
  let action = yield take(actions.CREATE_PLATFORM)
  while (action !== END) {
    const {authentication, values} = action
    yield fork(createPlatform, authentication, values)
    action = yield take(actions.CREATE_PLATFORM)
  }
}


function* watchDeletePlatform() {
  let action = yield take(actions.DELETE_PLATFORM)
  while (action !== END) {
    const {authentication, id} = action
    yield fork(deletePlatform, authentication, id)
    action = yield take(actions.DELETE_PLATFORM)
  }
}


function* watchLoadPlatform() {
  let action = yield take(actions.LOAD_PLATFORM)
  while (action !== END) {
    const {authentication, id} = action
    yield fork(loadPlatform, authentication, id)
    action = yield take(actions.LOAD_PLATFORM)
  }
}


function* watchLoadPlatforms() {
  let action = yield take(actions.LOAD_PLATFORMS)
  while (action !== END) {
    const {authentication} = action
    yield fork(loadPlatforms, authentication)
    action = yield take(actions.LOAD_PLATFORMS)
  }
}


function* watchUpdatePlatform() {
  let action = yield take(actions.UPDATE_PLATFORM)
  while (action !== END) {
    const {authentication, id, values} = action
    yield fork(updatePlatform, authentication, id, values)
    action = yield take(actions.UPDATE_PLATFORM)
  }
}


// Programs


function* createProgram(authentication, values) {
  yield put(actions.creatingProgram.request(authentication, values))
  try {
    const program = yield call(
      fetchApiJson,
      "programs",
      {
        body: JSON.stringify(values),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "post",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.creatingProgram.success(program))
    browserHistory.push(`/${language}/programs/${program.id}`)
  } catch (error) {
    yield put(actions.creatingProgram.failure(authentication, values, error))
  }
}


function* deleteProgram(authentication, id) {
  yield put(actions.deletingProgram.request(authentication, id))
  try {
    const program = yield call(
      fetchApiJson,
      `programs/${id}`,
      {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "delete",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.deletingProgram.success(program))
    browserHistory.push(`/${language}/programs`)
  } catch (error) {
    yield put(actions.deletingProgram.failure(authentication, id, error))
  }
}


export function* fetchProgram(authentication, id) {
  yield put(actions.loadingProgram.request(authentication, id))
  const authenticationHeaders = authentication && authentication.apiKey ?
    {"OGPToolbox-API-Key": authentication.apiKey} :
    {}
  try {
    const program = yield call(
      fetchApiJson,
      `programs/${id}`,
      {
        headers: {
          "Accept": "application/json",
          ...authenticationHeaders,
        },
      },
    )
    yield put(actions.loadingProgram.success(program))
  } catch (error) {
    yield put(actions.loadingProgram.failure(authentication, id, error))
  }
}


export function* fetchPrograms(authentication) {
  yield put(actions.loadingPrograms.request(authentication))
  const authenticationHeaders = authentication && authentication.apiKey ?
    {"OGPToolbox-API-Key": authentication.apiKey} :
    {}
  try {
    const programs = yield call(
      fetchApiJson,
      "programs",
      {
        headers: {
          "Accept": "application/json",
          ...authenticationHeaders,
        },
      },
    )
    Array.prototype.splice.apply(links.programsEnum,
      [0, links.programsEnum].concat(programs.map(program => program.name).sort()))
    yield put(actions.loadingPrograms.success(programs))
  } catch (error) {
    yield put(actions.loadingPrograms.failure(authentication, error))
  }
}


function getProgram(state, id) {
  // return state.programById && state.programById[id]
  return state.programById[id]
}


function getProgramIds(state) {
  return state.programIds
}


function* loadProgram(authentication, id) {
  const program = yield select(getProgram, id)
  if (!program) yield call(fetchProgram, authentication, id)
}


function* loadPrograms(authentication) {
  const programIds = yield select(getProgramIds)
  if (programIds === null || programIds.length === 0) yield call(fetchPrograms, authentication)
}


function* updateProgram(authentication, id, values) {
  values = {...values}
  for (let key in values) {
    if (values[key] === null || values[key] === undefined) delete values[key]
  }
  yield put(actions.updatingProgram.request(authentication, id, values))
  try {
    const program = yield call(
      fetchApiJson,
      `programs/${id}`,
      {
        body: JSON.stringify(values),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "put",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.updatingProgram.success(program))
    browserHistory.push(`/${language}/programs/${program.id}`)
  } catch (error) {
    yield put(actions.updatingProgram.failure(authentication, id, values, error))
  }
}


function* watchCreateProgram() {
  let action = yield take(actions.CREATE_PROGRAM)
  while (action !== END) {
    const {authentication, values} = action
    yield fork(createProgram, authentication, values)
    action = yield take(actions.CREATE_PROGRAM)
  }
}


function* watchDeleteProgram() {
  let action = yield take(actions.DELETE_PROGRAM)
  while (action !== END) {
    const {authentication, id} = action
    yield fork(deleteProgram, authentication, id)
    action = yield take(actions.DELETE_PROGRAM)
  }
}


function* watchLoadProgram() {
  let action = yield take(actions.LOAD_PROGRAM)
  while (action !== END) {
    const {authentication, id} = action
    yield fork(loadProgram, authentication, id)
    action = yield take(actions.LOAD_PROGRAM)
  }
}


function* watchLoadPrograms() {
  let action = yield take(actions.LOAD_PROGRAMS)
  while (action !== END) {
    const {authentication} = action
    yield fork(loadPrograms, authentication)
    action = yield take(actions.LOAD_PROGRAMS)
  }
}


function* watchUpdateProgram() {
  let action = yield take(actions.UPDATE_PROGRAM)
  while (action !== END) {
    const {authentication, id, values} = action
    yield fork(updateProgram, authentication, id, values)
    action = yield take(actions.UPDATE_PROGRAM)
  }
}


// Tags


function* createTag(authentication, values) {
  yield put(actions.creatingTag.request(authentication, values))
  try {
    const tag = yield call(
      fetchApiJson,
      "tags",
      {
        body: JSON.stringify(values),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "post",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.creatingTag.success(tag))
    browserHistory.push(`/${language}/tags/${tag.id}`)
  } catch (error) {
    yield put(actions.creatingTag.failure(authentication, values, error))
  }
}


function* deleteTag(authentication, id) {
  yield put(actions.deletingTag.request(authentication, id))
  try {
    const tag = yield call(
      fetchApiJson,
      `tags/${id}`,
      {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "delete",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.deletingTag.success(tag))
    browserHistory.push(`/${language}/tags`)
  } catch (error) {
    yield put(actions.deletingTag.failure(authentication, id, error))
  }
}


export function* fetchTag(authentication, id) {
  yield put(actions.loadingTag.request(authentication, id))
  const authenticationHeaders = authentication && authentication.apiKey ?
    {"OGPToolbox-API-Key": authentication.apiKey} :
    {}
  try {
    const tag = yield call(
      fetchApiJson,
      `tags/${id}`,
      {
        headers: {
          "Accept": "application/json",
          ...authenticationHeaders,
        },
      },
    )
    yield put(actions.loadingTag.success(tag))
  } catch (error) {
    yield put(actions.loadingTag.failure(authentication, id, error))
  }
}


export function* fetchTags(authentication) {
  yield put(actions.loadingTags.request(authentication))
  const authenticationHeaders = authentication && authentication.apiKey ?
    {"OGPToolbox-API-Key": authentication.apiKey} :
    {}
  try {
    const tags = yield call(
      fetchApiJson,
      "tags",
      {
        headers: {
          "Accept": "application/json",
          ...authenticationHeaders,
        },
      },
    )
    Array.prototype.splice.apply(links.tagsEnum,
      [0, links.tagsEnum].concat(tags.map(tag => tag.name).sort()))
    yield put(actions.loadingTags.success(tags))
  } catch (error) {
    yield put(actions.loadingTags.failure(authentication, error))
  }
}


function getTag(state, id) {
  // return state.tagById && state.tagById[id]
  return state.tagById[id]
}


function getTagIds(state) {
  return state.tagIds
}


function* loadTag(authentication, id) {
  const tag = yield select(getTag, id)
  if (!tag) yield call(fetchTag, authentication, id)
}


function* loadTags(authentication) {
  const tagIds = yield select(getTagIds)
  if (tagIds === null || tagIds.length === 0) yield call(fetchTags, authentication)
}


function* updateTag(authentication, id, values) {
  values = {...values}
  for (let key in values) {
    if (values[key] === null || values[key] === undefined) delete values[key]
  }
  yield put(actions.updatingTag.request(authentication, id, values))
  try {
    const tag = yield call(
      fetchApiJson,
      `tags/${id}`,
      {
        body: JSON.stringify(values),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "put",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.updatingTag.success(tag))
    browserHistory.push(`/${language}/tags/${tag.id}`)
  } catch (error) {
    yield put(actions.updatingTag.failure(authentication, id, values, error))
  }
}


function* watchCreateTag() {
  let action = yield take(actions.CREATE_TAG)
  while (action !== END) {
    const {authentication, values} = action
    yield fork(createTag, authentication, values)
    action = yield take(actions.CREATE_TAG)
  }
}


function* watchDeleteTag() {
  let action = yield take(actions.DELETE_TAG)
  while (action !== END) {
    const {authentication, id} = action
    yield fork(deleteTag, authentication, id)
    action = yield take(actions.DELETE_TAG)
  }
}


function* watchLoadTag() {
  let action = yield take(actions.LOAD_TAG)
  while (action !== END) {
    const {authentication, id} = action
    yield fork(loadTag, authentication, id)
    action = yield take(actions.LOAD_TAG)
  }
}


function* watchLoadTags() {
  let action = yield take(actions.LOAD_TAGS)
  while (action !== END) {
    const {authentication} = action
    yield fork(loadTags, authentication)
    action = yield take(actions.LOAD_TAGS)
  }
}


function* watchUpdateTag() {
  let action = yield take(actions.UPDATE_TAG)
  while (action !== END) {
    const {authentication, id, values} = action
    yield fork(updateTag, authentication, id, values)
    action = yield take(actions.UPDATE_TAG)
  }
}


// Usages


function* createUsage(authentication, values) {
  yield put(actions.creatingUsage.request(authentication, values))
  try {
    const usage = yield call(
      fetchApiJson,
      "usages",
      {
        body: JSON.stringify(values),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "post",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.creatingUsage.success(usage))
    browserHistory.push(`/${language}/usages/${usage.id}`)
  } catch (error) {
    yield put(actions.creatingUsage.failure(authentication, values, error))
  }
}


function* deleteUsage(authentication, id) {
  yield put(actions.deletingUsage.request(authentication, id))
  try {
    const usage = yield call(
      fetchApiJson,
      `usages/${id}`,
      {
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "delete",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.deletingUsage.success(usage))
    browserHistory.push(`/${language}/usages`)
  } catch (error) {
    yield put(actions.deletingUsage.failure(authentication, id, error))
  }
}


export function* fetchUsage(authentication, id) {
  yield put(actions.loadingUsage.request(authentication, id))
  const authenticationHeaders = authentication && authentication.apiKey ?
    {"OGPToolbox-API-Key": authentication.apiKey} :
    {}
  try {
    const usage = yield call(
      fetchApiJson,
      `usages/${id}`,
      {
        headers: {
          "Accept": "application/json",
          ...authenticationHeaders,
        },
      },
    )
    yield put(actions.loadingUsage.success(usage))
  } catch (error) {
    yield put(actions.loadingUsage.failure(authentication, id, error))
  }
}


export function* fetchUsages(authentication) {
  yield put(actions.loadingUsages.request(authentication))
  const authenticationHeaders = authentication && authentication.apiKey ?
    {"OGPToolbox-API-Key": authentication.apiKey} :
    {}
  try {
    const usages = yield call(
      fetchApiJson,
      "usages",
      {
        headers: {
          "Accept": "application/json",
          ...authenticationHeaders,
        },
      },
    )
    Array.prototype.splice.apply(links.usagesEnum,
      [0, links.usagesEnum].concat(usages.map(usage => usage.name).sort()))
    yield put(actions.loadingUsages.success(usages))
  } catch (error) {
    yield put(actions.loadingUsages.failure(authentication, error))
  }
}


function getUsage(state, id) {
  // return state.usageById && state.usageById[id]
  return state.usageById[id]
}


function getUsageIds(state) {
  return state.usageIds
}


function* loadUsage(authentication, id) {
  const usage = yield select(getUsage, id)
  if (!usage) yield call(fetchUsage, authentication, id)
}


function* loadUsages(authentication) {
  const usageIds = yield select(getUsageIds)
  if (usageIds === null || usageIds.length === 0) yield call(fetchUsages, authentication)
}


function* updateUsage(authentication, id, values) {
  values = {...values}
  for (let key in values) {
    if (values[key] === null || values[key] === undefined) delete values[key]
  }
  yield put(actions.updatingUsage.request(authentication, id, values))
  try {
    const usage = yield call(
      fetchApiJson,
      `usages/${id}`,
      {
        body: JSON.stringify(values),
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "OGPToolbox-API-Key": authentication.apiKey,
        },
        method: "put",
      },
    )
    const language = yield select(getLanguage)
    yield put(actions.updatingUsage.success(usage))
    browserHistory.push(`/${language}/usages/${usage.id}`)
  } catch (error) {
    yield put(actions.updatingUsage.failure(authentication, id, values, error))
  }
}


function* watchCreateUsage() {
  let action = yield take(actions.CREATE_USAGE)
  while (action !== END) {
    const {authentication, values} = action
    yield fork(createUsage, authentication, values)
    action = yield take(actions.CREATE_USAGE)
  }
}


function* watchDeleteUsage() {
  let action = yield take(actions.DELETE_USAGE)
  while (action !== END) {
    const {authentication, id} = action
    yield fork(deleteUsage, authentication, id)
    action = yield take(actions.DELETE_USAGE)
  }
}


function* watchLoadUsage() {
  let action = yield take(actions.LOAD_USAGE)
  while (action !== END) {
    const {authentication, id} = action
    yield fork(loadUsage, authentication, id)
    action = yield take(actions.LOAD_USAGE)
  }
}


function* watchLoadUsages() {
  let action = yield take(actions.LOAD_USAGES)
  while (action !== END) {
    const {authentication} = action
    yield fork(loadUsages, authentication)
    action = yield take(actions.LOAD_USAGES)
  }
}


function* watchUpdateUsage() {
  let action = yield take(actions.UPDATE_USAGE)
  while (action !== END) {
    const {authentication, id, values} = action
    yield fork(updateUsage, authentication, id, values)

    action = yield take(actions.UPDATE_USAGE)
  }
}

//


export default function* root() {
  yield [
    call(watchLoadAuthenticationCookie),
    call(watchLoadEverything),
    call(watchSignIn),
    call(watchSignOut),
    call(watchSignUp),

    call(watchCreateOrganization),
    call(watchDeleteOrganization),
    call(watchLoadOrganization),
    call(watchLoadOrganizations),
    call(watchUpdateOrganization),

    call(watchCreatePlatform),
    call(watchDeletePlatform),
    call(watchLoadPlatform),
    call(watchLoadPlatforms),
    call(watchUpdatePlatform),

    call(watchCreateProgram),
    call(watchDeleteProgram),
    call(watchLoadProgram),
    call(watchLoadPrograms),
    call(watchUpdateProgram),

    call(watchCreateTag),
    call(watchDeleteTag),
    call(watchLoadTag),
    call(watchLoadTags),
    call(watchUpdateTag),

    call(watchCreateUsage),
    call(watchDeleteUsage),
    call(watchLoadUsage),
    call(watchLoadUsages),
    call(watchUpdateUsage),
  ]
}
