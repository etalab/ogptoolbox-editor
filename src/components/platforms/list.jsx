// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {Component, PropTypes} from "react"
import {connect} from "react-redux"
import {Link} from "react-router"

import {loadEverything} from "../../actions"
import PlatformLine from "./line"


class PlatformsList extends Component {
  static propTypes = {
    authentication: PropTypes.object,
    language: PropTypes.string.isRequired,
    loadEverything: PropTypes.func.isRequired,
    platforms: PropTypes.arrayOf(PropTypes.object),
  }
  componentWillMount() {
    this.props.loadEverything(this.props.authentication)
  }
  render() {
    const {authentication, language, platforms} = this.props
    return (
      <section>
        <h1>Platforms</h1>
        {platforms.map(platform => <PlatformLine key={platform.id} platform={platform} />)}
        {authentication && Object.keys(authentication).length > 0 ? (
          <Link className="btn btn-default" role="button" to={`/${language}/platforms/new`}>New Platform</Link>
        ) : null}
      </section>
    )
  }
}

export default connect(
  state => ({
    authentication: state.authentication,
    language: state.language,
    platforms: state.platformIds ? state.platformIds.map(platformId => state.platformById[platformId]) : [],
  }),
  {
    loadEverything,
  },
)(PlatformsList)