// OGPToolbox-Editor -- Web editor for OGP toolbox
// By: Emmanuel Raviart <emmanuel.raviart@data.gouv.fr>
//
// Copyright (C) 2016 Etalab
// https://git.framasoft.org/etalab/ogptoolbox-editor
//
// OGPToolbox-Editor is free software; you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// OGPToolbox-Editor is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


import {Component, PropTypes} from "react"
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap"
import {connect} from "react-redux"


class Profile extends Component {
  static propTypes = {
    authentication: PropTypes.object.isRequired,
  }
  render() {
    const {authentication} = this.props
    if (!authentication) return (
      <p>Loading user profile...</p>
    )
    return (
      <section>
        <FormGroup>
          <ControlLabel>Name</ControlLabel>
          <FormControl.Static>
            {authentication.name}
          </FormControl.Static>
        </FormGroup>
        <FormGroup>
          <ControlLabel>Email</ControlLabel>
          <FormControl.Static>
            {authentication.email}
          </FormControl.Static>
        </FormGroup>
        <FormGroup>
          <ControlLabel>API Key</ControlLabel>
          <FormControl.Static>
            {authentication.apiKey}
          </FormControl.Static>
        </FormGroup>
        <pre>{JSON.stringify(authentication, null, 2)}</pre>
      </section>
    )
  }
}

export default connect(
  state => ({
    authentication: state.authentication,
  }),
)(Profile)